﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Application.Services;
using Autofac;

namespace AirportApiDistance.WebApiApp.CompositionRoot
{
    public class ServicesModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<DistanceCalculator>()
                .As<IDistanceCalculator>()
                .SingleInstance();
        }
    }
}