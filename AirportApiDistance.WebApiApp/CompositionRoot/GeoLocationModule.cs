﻿using AirportApiDistance.Infra.GeoLocation.CompositionRoot;
using Autofac;

namespace AirportApiDistance.WebApiApp.CompositionRoot
{
    public class GeoLocationModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeoLocationDependencies();
        }
    }
}