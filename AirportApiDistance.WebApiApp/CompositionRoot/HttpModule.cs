﻿using AirportApiDistance.Infra.Http.CompositionRoot;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace AirportApiDistance.WebApiApp.CompositionRoot
{
    public class HttpModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterHttpDependencies(context =>
            {
                var configuration = context.Resolve<IConfiguration>();
                var airportsUri = configuration.GetValue<string>("AirportsUri");

                return airportsUri;
            });
        }
    }
}