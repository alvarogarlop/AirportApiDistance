﻿using AirportApiDistance.WebApiApp.Swagger;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace AirportApiDistance.WebApiApp.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(x =>
            {
                var info =
                    new Info
                    {
                        Title = SwaggerConfiguration.InfoTitle,
                        Version = SwaggerConfiguration.InfoVersion
                    };

                x.SwaggerDoc(SwaggerConfiguration.DocNameV1, info);
            });

            return services;
        }
    }
}