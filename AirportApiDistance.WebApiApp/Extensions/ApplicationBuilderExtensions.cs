﻿using AirportApiDistance.WebApiApp.Swagger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace AirportApiDistance.WebApiApp.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger(option => { option.RouteTemplate = SwaggerConfiguration.JsonRoute; });
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint(SwaggerConfiguration.UiEndPoint, SwaggerConfiguration.Description);
            });

            return app;
        }

        public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(x =>
            {
                x.Run(
                    async context =>
                    {
                        var handler = context.Features.Get<IExceptionHandlerFeature>();
                        var exception = handler?.Error;
                        if (exception is null)
                        {
                            return;
                        }

                        context.Response.ContentType = "text/html";
                        context.Response.StatusCode = (int) HttpStatusCode.NotFound;
                        await context.Response.WriteAsync(exception.Message).ConfigureAwait(false);
                    });
            });

            return app;
        }
    }
}