﻿namespace AirportApiDistance.WebApiApp.Swagger
{
    public class SwaggerConfiguration
    {
        internal const string JsonRoute = "swagger/{documentName}/swagger.json";
        internal const string Description = "AirportApiDistance Swagger";
        internal const string UiEndPoint = "v1/swagger.json";
        internal const string DocNameV1 = "v1";
        internal const string InfoTitle = "AirportApiDistance Swagger";
        internal const string InfoVersion = "v1";
    }
}