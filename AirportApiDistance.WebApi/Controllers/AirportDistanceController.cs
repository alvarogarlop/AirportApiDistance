﻿using AirportApiDistance.Application.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace AirportApiDistance.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class AirportDistanceController
        : Controller
    {
        private IDistanceCalculator _distanceCalculator;

        public AirportDistanceController(IDistanceCalculator distanceCalculator)
        {
            _distanceCalculator = distanceCalculator ?? throw new ArgumentNullException(nameof(distanceCalculator));
        }

        [HttpGet("")]
        public async Task<IActionResult> GetDistance(string iataCodeA, string iataCodeB)
        {
            var distance = await _distanceCalculator.CalculateDistance(iataCodeA, iataCodeB);

            return Ok(
                new
                {
                    AirportA = iataCodeA,
                    AirportB = iataCodeB,
                    distance
                }
            );
        }
    }
}