﻿namespace AirportApiDistance.Application.Models
{
    public class Airport
    {
        public string Iata { get; set; }
        public Location Location { get; set; }
    }
}