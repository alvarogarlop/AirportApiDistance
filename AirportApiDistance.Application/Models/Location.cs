﻿namespace AirportApiDistance.Application.Models
{
    public class Location
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }
}