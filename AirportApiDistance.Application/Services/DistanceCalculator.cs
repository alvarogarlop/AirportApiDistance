﻿using AirportApiDistance.Application.Contracts;
using System;
using System.Threading.Tasks;

namespace AirportApiDistance.Application.Services
{
    public class DistanceCalculator
        : IDistanceCalculator
    {
        private readonly IGetService _getService;
        private readonly IGeoLocationDistanceCalculator _geoLocationDistanceCalculator;

        public DistanceCalculator(IGetService getService, IGeoLocationDistanceCalculator geoLocationDistanceCalculator)
        {
            _getService = getService ?? throw new ArgumentNullException(nameof(getService));
            _geoLocationDistanceCalculator = geoLocationDistanceCalculator ?? throw new ArgumentNullException(nameof(geoLocationDistanceCalculator));
        }

        public async Task<double> CalculateDistance(string aiataCodeA, string aiataCodeB)
        {
            var airports = await _getService.GetAirports(aiataCodeA, aiataCodeB);
            var locationA = airports[0].Location;
            var locationB = airports[1].Location;

            var distance = _geoLocationDistanceCalculator.CalculateDistance(locationA, locationB);

            return distance;
        }
    }
}