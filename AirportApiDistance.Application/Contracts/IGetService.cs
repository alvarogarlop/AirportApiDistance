﻿using AirportApiDistance.Application.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AirportApiDistance.Application.Contracts
{
    public interface IGetService
    {
        Task<List<Airport>> GetAirports(params string[] iataCodes);
    }
}