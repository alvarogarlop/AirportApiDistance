﻿using System.Threading.Tasks;

namespace AirportApiDistance.Application.Contracts
{
    public interface IDistanceCalculator
    {
        Task<double> CalculateDistance(string aiataCodeA, string aiataCodeB);
    }
}