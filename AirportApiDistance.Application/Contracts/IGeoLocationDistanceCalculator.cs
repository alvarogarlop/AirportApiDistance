﻿using AirportApiDistance.Application.Models;

namespace AirportApiDistance.Application.Contracts
{
    public interface IGeoLocationDistanceCalculator
    {
        double CalculateDistance(Location a, Location b);
    }
}