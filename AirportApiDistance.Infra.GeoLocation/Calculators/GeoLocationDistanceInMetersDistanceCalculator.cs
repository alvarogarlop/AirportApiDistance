﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Application.Models;
using GeoCoordinatePortable;

namespace AirportApiDistance.Application.Services
{
    public class GeoLocationDistanceInMetersDistanceCalculator
        : IGeoLocationDistanceCalculator
    {
        public double CalculateDistance(Location a, Location b)
        {
            var coordinateA = new GeoCoordinate(a.lat, a.lon);
            var coordinateB = new GeoCoordinate(b.lat, b.lon);

            var distance = coordinateA.GetDistanceTo(coordinateB);
            return distance;
        }
    }
}