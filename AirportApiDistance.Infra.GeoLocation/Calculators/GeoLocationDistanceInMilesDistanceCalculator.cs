﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Application.Models;
using System;

namespace AirportApiDistance.Application.Services
{
    public class GeoLocationDistanceInMilesDistanceCalculator
        : IGeoLocationDistanceCalculator
    {
        private readonly IGeoLocationDistanceCalculator _geoLocationDistanceCalculator;
        private const double MetersPerMile = 1609.344;

        public GeoLocationDistanceInMilesDistanceCalculator(IGeoLocationDistanceCalculator geoLocationDistanceCalculator)
        {
            _geoLocationDistanceCalculator = geoLocationDistanceCalculator ?? throw new ArgumentNullException(nameof(geoLocationDistanceCalculator));
        }

        public double CalculateDistance(Location a, Location b)
        {
            var meters = _geoLocationDistanceCalculator.CalculateDistance(a, b);
            var miles = meters / MetersPerMile;

            return miles;
        }
    }
}