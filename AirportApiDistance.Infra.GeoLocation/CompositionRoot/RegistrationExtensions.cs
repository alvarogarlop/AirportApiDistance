﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Application.Services;
using Autofac;

namespace AirportApiDistance.Infra.GeoLocation.CompositionRoot
{
    public static class RegistrationExtensions
    {
        public static void RegisterGeoLocationDependencies(this ContainerBuilder builder)
        {
            builder
                .RegisterType<GeoLocationDistanceInMetersDistanceCalculator>()
                .As<IGeoLocationDistanceCalculator>()
                .SingleInstance();

            builder
                .RegisterDecorator<GeoLocationDistanceInMilesDistanceCalculator, IGeoLocationDistanceCalculator>();
        }
    }
}