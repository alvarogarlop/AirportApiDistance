﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Application.Models;
using AirportApiDistance.Application.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace AirportApiDistance.Infra.GeoLocation.Tests.Calculators
{
    public class GeoLocationDistanceInMilesCalculatorTests
    {
        private GeoLocationDistanceInMilesDistanceCalculator _sut;
        private const double MetersPerMile = 1609.344;

        [Fact]
        public void Given_Two_Locations_When_Calculating_The_Distance_In_Miles_Then_It_Should_Calculate_The_Distance_In_Meters()
        {
            var locationA = new Location();
            var locationB = new Location();

            var geoLocationDistanceInMetersCalculatorMock = new Mock<IGeoLocationDistanceCalculator>();
            var geoLocationDistanceInMetersCalculator = geoLocationDistanceInMetersCalculatorMock.Object;
            _sut = new GeoLocationDistanceInMilesDistanceCalculator(geoLocationDistanceInMetersCalculator);

            _sut.CalculateDistance(locationA, locationB);

            geoLocationDistanceInMetersCalculatorMock.Verify(x => x.CalculateDistance(locationA, locationB));
        }

        [Fact]
        public void Given_Two_Locations_When_Calculating_The_Distance_In_Miles_Then_It_Should_Return_Expected_Miles()
        {
            var locationA = new Location();
            var locationB = new Location();

            const double calculatedDistanceInMeters = 3218.688;
            var geoLocationDistanceInMetersCalculatorMock = new Mock<IGeoLocationDistanceCalculator>();
            geoLocationDistanceInMetersCalculatorMock
                .Setup(x => x.CalculateDistance(locationA, locationB))
                .Returns(calculatedDistanceInMeters);

            var geoLocationDistanceInMetersCalculator = geoLocationDistanceInMetersCalculatorMock.Object;
            _sut = new GeoLocationDistanceInMilesDistanceCalculator(geoLocationDistanceInMetersCalculator);

            var result = _sut.CalculateDistance(locationA, locationB);

            var expectedMiles = calculatedDistanceInMeters / MetersPerMile;
            result.Should().Be(expectedMiles);

        }
    }
}