﻿using AirportApiDistance.Application.Models;
using AirportApiDistance.Application.Services;
using FluentAssertions;
using Xunit;

namespace AirportApiDistance.Infra.GeoLocation.Tests.Calculators
{
    public class GeoLocationDistanceInMetersCalculatorTests
    {
        private GeoLocationDistanceInMetersDistanceCalculator _sut;

        [Theory]
        [InlineData(37.423476, -5.900136, 36.675181, -4.489616, 150436.72366792083)]
        [InlineData(40.49027, -3.564479, 41.303027, 2.07593, 482944.25762956723)]
        public void Given_A_Location_With_LatitudeA_And_LongitudeA_And_Another_Location_With_LatitudeB_And_Longitude_B_When_Calculating_The_Distance_In_Meters_Then_It_Should_Return_Expected_Result(
                double latitudeA, 
                double longitudeA, 
                double latitudeB, 
                double longitudeB,
                double expectedResult)
        {
            var locationA = CreateLocation(latitudeA, longitudeA);
            var locationB = CreateLocation(latitudeB, longitudeB);

            _sut = new GeoLocationDistanceInMetersDistanceCalculator();
            var result = _sut.CalculateDistance(locationA, locationB);

            result.Should().Be(expectedResult);
        }

        private static Location CreateLocation(double latitude, double longitude)
        {
            var location =
                new Location
                {
                    lat = latitude,
                    lon = longitude
                };

            return location;
        }
    }
}