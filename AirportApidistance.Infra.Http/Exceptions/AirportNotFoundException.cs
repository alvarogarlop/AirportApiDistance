﻿using System;

namespace AirportApiDistance.Infra.Http.Exceptions
{
    public class AirportNotFoundException
        : Exception
    {
        public AirportNotFoundException(string iataCode)
            : base($"Airport with iata code {iataCode} not found")
        {
        }
    }
}