﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Infra.Http.Services;
using Autofac;
using System;

namespace AirportApiDistance.Infra.Http.CompositionRoot
{
    public static class RegistrationExtensions
    {
        public static void RegisterHttpDependencies(this ContainerBuilder builder, Func<IComponentContext, string> airportsUriRetriever)
        {
            builder
                .Register(context =>
                {
                    var componentContext = context.Resolve<IComponentContext>();
                    var airportsUri = airportsUriRetriever.Invoke(componentContext);

                    var httpGetService = new HttpGetService(airportsUri);
                    return httpGetService;
                })
                .As<IGetService>()
                .SingleInstance();
        }
    }
}