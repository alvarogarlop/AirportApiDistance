﻿using AirportApiDistance.Application.Contracts;
using AirportApiDistance.Application.Models;
using AirportApiDistance.Infra.Http.Exceptions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AirportApiDistance.Infra.Http.Services
{
    public class HttpGetService
        : IGetService
    {
        private readonly string _airportsUri;

        public HttpGetService(string airportsUri)
        {
            _airportsUri = airportsUri;
        }

        public async Task<List<Airport>> GetAirports(params string[] iataCodes)
        {
            var airports = new List<Airport>();

            using (var httpClient = new HttpClient())
            {
                foreach (var iataCode in iataCodes)
                {
                    var uri = string.Format(_airportsUri, iataCode);
                    var response = await httpClient.GetAsync(uri);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new AirportNotFoundException(iataCode);
                    }
                    var responseContent = await response.Content.ReadAsStringAsync();

                    var airport = JsonConvert.DeserializeObject<Airport>(responseContent);
                    airports.Add(airport);
                }
            }

            return airports;
        }
    }
}